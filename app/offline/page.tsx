import { Avatar, IconButton, Tooltip } from "@mui/material";
import React from "react";

type Props = {};

const offline = (props: Props) => {
  return (
    <div>
      <Tooltip title="Open settings">
        <Avatar alt="Remy Sharp" src="/images/avatar/avatar.png" />
      </Tooltip>
    </div>
  );
};

export default offline;
