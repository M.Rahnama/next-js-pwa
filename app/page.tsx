import {
  Button,
  Card,
  CardContent,
  Container,
  Grid,
  Paper,
  TextField,
} from "@mui/material";

export default function Home() {
  return (
    <main>
      <Container maxWidth="sm">
        <Grid container sx={{ mt: "1.5rem" }}>
          <Grid item xs={12}>
            <Card>
              <CardContent>
                <form>
                  <Grid
                    container
                    sx={{ display: "flex", gap: 2, flexFlow: "column" }}
                  >
                    <Grid item xs={12}>
                      <TextField
                        sx={{ width: "100%" }}
                        id="outlined-basic"
                        label="Outlined"
                        variant="outlined"
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <TextField
                        sx={{ width: "100%" }}
                        id="outlined-basic"
                        label="Outlined"
                        variant="outlined"
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <Button variant="contained" sx={{ width: "100%" }}>
                        Contained
                      </Button>
                    </Grid>
                  </Grid>
                </form>
              </CardContent>
            </Card>
          </Grid>
        </Grid>
      </Container>
    </main>
  );
}
