import pwa from "next-pwa";
/** @type {import('next').NextConfig} */

const nextConfig = {};

const withPWA = pwa({
  // disable: process.env.NODE_ENV === "development",
  register: true,
  cacheStartUrl: true,
  skipWaiting: true,
  cacheOnFrontEndNav: true,
  reloadOnOnline: true,
  chunks: ["1"],
  dest: "public",
});
/** @type {import('next').NextConfig} */

export default withPWA(nextConfig);
